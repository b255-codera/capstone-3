import React from "react";
import { useContext, useState } from "react";
import { Link, NavLink } from "react-router-dom";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";
import UserContext from "../UserContext";
import { useSelector } from "react-redux";

const Navbar = () => {
  const { cartTotalQuantity } = useSelector((state) => state.cart);
  const [color, setColor] = useState(false);
  const { user } = useContext(UserContext);

  const changeColor = () => {
    if (window.scrollY >= 90) {
      setColor(true);
    } else {
      setColor(false);
    }
  };

  window.addEventListener("scroll", changeColor);

  return (
    <div>
      <nav
        class={
          color
            ? "navbar navbar-expand-lg navbar-bg"
            : "navbar navbar-expand-lg bg-white"
        }
      >
        <div class="container">
          <div className="logo">
            <a class="navbar-brand" href="/">
              <img src="../img/logo.png" className="img-fluid" alt="logo" />
            </a>
          </div>

          <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <NavLink to="/" className="px-4 nav-link" aria-current="page">
                  Home
                </NavLink>
              </li>
              <li class="nav-item dropdown">
                <Link
                  className="px-4 nav-link dropdown-toggle"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Perfumes
                </Link>
                <ul class="dropdown-menu">
                  <li>
                    <NavLink
                      to="/collections/fragrance-for-men"
                      className="dropdown-item"
                      href="#"
                    >
                      Men
                    </NavLink>
                  </li>
                  <li>
                    <NavLink
                      to="/collections/fragrance-for-women"
                      className="dropdown-item"
                    >
                      Women
                    </NavLink>
                  </li>
                </ul>
              </li>
              <li class="nav-item">
                <NavLink to="/collections/all" className="px-4 nav-link">
                  All Collections
                </NavLink>
              </li>
            </ul>
            <div className="icons">
              <SearchIcon
                sx={{ stroke: "#ffffff", strokeWidth: 1 }}
                className="fs-2 text-dark search-icon"
              />
              {user.id !== null ? (
                <Link to="/account">
                  <PersonOutlineOutlinedIcon
                    sx={{ stroke: "#ffffff", strokeWidth: 1 }}
                    className="fs-2 text-dark account-icon"
                  />
                </Link>
              ) : (
                <Link to="/account/login">
                  <PersonOutlineOutlinedIcon
                    sx={{ stroke: "#ffffff", strokeWidth: 1 }}
                    className="fs-2 text-dark account-icon"
                  />
                </Link>
              )}
              <div className="cartIcon">
                <Link to="/cart">
                  <ShoppingCartOutlinedIcon
                    sx={{ stroke: "#ffffff", strokeWidth: 1 }}
                    className="fs-2 text-dark cart-icon"
                  />
                </Link>
                <span>{cartTotalQuantity}</span>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
