import React from "react";
import { Link } from "react-router-dom";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import LocalPhoneOutlinedIcon from "@mui/icons-material/LocalPhoneOutlined";
import FacebookOutlinedIcon from "@mui/icons-material/FacebookOutlined";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";

const Footer = () => {
  return (
    <>
      <footer className="bg-dark py-5">
        <div className="container">
          <div className="row text-white g-4">
            <div className="col-md-6 col-lg-3">
              <Link
                className="text-uppercase text-decoration-none brand text-white"
                to="/"
              >
                JGC Scents
              </Link>
              <p className="text-white text-muted mt-3">
                JGC Scents offers genuine perfumes at great discounted prices.
                All trademarks are property of their respective owners.
              </p>
            </div>
            <div className="col-md-6 col-lg-3">
              <h5 className="fw-light">Quick Links</h5>
              <ul className="list-unstyled ps-0">
                <li className="my-3">
                  <Link
                    to="/"
                    className="text-white text-decoration-none text-muted"
                  >
                    Home
                  </Link>
                </li>
                <li className="my-3">
                  <Link
                    to="/collections/all"
                    className="text-white text-decoration-none text-muted"
                  >
                    Collection
                  </Link>
                </li>
                <li className="my-3">
                  <Link
                    to="/"
                    className="text-white text-decoration-none text-muted"
                  >
                    About Us
                  </Link>
                </li>
                <li className="my-3">
                  <Link
                    to="/collections/fragrance-for-men"
                    className="text-white text-decoration-none text-muted"
                  >
                    For Him
                  </Link>
                </li>
                <li className="my-3">
                  <Link
                    to="/collections/fragrance-for-women"
                    className="text-white text-decoration-none text-muted"
                  >
                    For Her
                  </Link>
                </li>
              </ul>
            </div>
            <div className="col-md-6 col-lg-3">
              <h5 className="fw-light mb-3">Contact Us</h5>
              <div className="d-flex justify-content-start align-items-start my-2 text-muted">
                <span className="me-2">
                  <HomeOutlinedIcon />
                </span>
                <span className="fw-light">
                  Cebu City, Cebu, Philippines 6000
                </span>
              </div>
              <div className="d-flex justify-content-start align-items-start my-2 text-muted">
                <span className="me-2">
                  <EmailOutlinedIcon />
                </span>
                <span className="fw-light">jgcscents.support@gmail.com</span>
              </div>
              <div className="d-flex justify-content-start align-items-start my-2 text-muted">
                <span className="me-2">
                  <LocalPhoneOutlinedIcon />
                </span>
                <span className="fw-light">+639 1234 56789</span>
              </div>
            </div>
            <div className="col-md-6 col-lg-3">
              <h5 className="fw-light mb-3">Follow Us</h5>
              <div>
                <ul className="list-unstyled d-flex">
                  <li>
                    <Link
                      to=""
                      className="text-white text-decoration-none text-muted fs-4 me-3"
                    >
                      <FacebookOutlinedIcon />
                    </Link>
                  </li>
                  <li>
                    <Link
                      to=""
                      className="text-white text-decoration-none text-muted fs-4 me-3"
                    >
                      <TwitterIcon />
                    </Link>
                  </li>
                  <li>
                    <Link
                      to=""
                      className="text-white text-decoration-none text-muted fs-4 me-3"
                    >
                      <InstagramIcon />
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
