import React from "react";
import Header from "../components/Header";
import HomeProducts from "../components/HomeProducts";
import Banner from "../components/Banner";
import Service from "../components/Service";

const Home = () => {
  return (
    <>
      <Header />
      <HomeProducts />
      <Banner />
      <Service />
    </>
  );
};

export default Home;
