export const generatePublicUrl = (filename) => {
  return `http://localhost:4000/uploads/${filename}`;
};
